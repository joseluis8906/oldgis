var express = require('express')
var router = express.Router()

var bcrypt = require('bcrypt');
var User = require('../models/User');
var UserBasicInfo = require('../models/UserBasicInfo');
var UserComplementaryInfo = require('../models/UserComplementaryInfo');

// define the home page route
router.post('/Select/', function (req, res) {

    var Data = req.body;

    User.findOne({ where: {UserName: Data.UserName}})
    .then(R => {
      if (R !== null) {
        bcrypt.compare(Data.Password, R.Password, function(err, result) {
            if(result){
              res.json({Result: 1});
            }
            else {
              res.json({Result: 0});
            }
        });
      }
      else {
        res.json({Result: 0});
      }
    });
});

router.post('/Insert/', function (req, res) {

    var Data = req.body;

    bcrypt.genSalt(10, function(err, salt){

      bcrypt.hash(Data.Password, salt, function(err, bcryptedPassword){

        User.create({
          UserName: Data.UserName,
          Password: bcryptedPassword,
          UserBasicInfo: {
            DocumentType: Data.DocumentType,
            DocumentNum: Data.DocumentNum,
            Country: Data.Country,
            Name: Data.Name,
            LastName: Data.LastName
          },
          UserComplementaryInfo: {
            Avatar: Data.Avatar,
            Phone: Data.Phone,
            Email: Data.Email,
            Address: Data.Address
          }
        },
        {
          include: [UserBasicInfo, UserComplementaryInfo]
        })
        .then(() => {
          res.json({Result: 1});
        });
      });
    });
});

router.post('/Update/', function (req, res) {

    var Data = req.body;

    User.findOne({ where: {UserName: Data.UserName}, include: [UserBasicInfo, UserComplementaryInfo]})
    .then(R =>{

      R.UserBasicInfo = {
        DocumentType: Data.DocumentType,
        DocumentNum: Data.DocumentNum,
        Country: Data.Country,
        Name: Data.Name,
        LastName: Data.LastName
      };

      R.UserComplementaryInfo = {
        Avatar: Data.Avatar,
        Phone: Data.Phone,
        Email: Data.Email,
        Address: Data.Address
      };

      R.UserBasicInfo.save();
      R.UserComplementaryInfo.save();

    });
});

router.post('/Delete/', function (req, res) {

  var Data = req.body;

  User.findOne({ where: {UserName: Data.UserName}})
  .then(R => {
     R.destroy();
  });
});

module.exports = router
