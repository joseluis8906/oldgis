var express = require('express')
var router = express.Router()

var bcrypt = require('bcrypt');
var User = require('../models/User');

router.post('/Start/', function (req, res, next) {
    var data = (req.body);

    User.findOne({ where: {UserName: data.UserName}})
    .then(R => {
      if (R !== null) {
        bcrypt.compare(data.Password, R.Password, function(err, res) {
            if(res){
              res.json({Result: 1});
            }
            else {
              res.json({Result: 0});
            }
        });
      }
      else {
        res.json({Result: 0});
      }
    });
});

router.post('/Renew/', function (req, res, next) {
    var data = (req.body);

    if (req.session.user)
    {
      res.json({Result: 1});
    }
    else
    {
      res.json({Result: 0});
    }
});

router.post('/Terminate/', function (req, res, next) {
    var data = (req.body);

    req.session.user = undefined;
    res.json({Result: 1});
});

module.exports = router
