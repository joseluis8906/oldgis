var express = require('express')
var multer = require('multer');
var router = express.Router();

// define the home page route
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, '../../uploads');
  },

  filename: function (req, file, callback) {
    var DocumentsExt = ["txt", "pdf", "ps", "rtf", "wps", "xml", "xps", "odt", "doc", "docm", "docx", "dot", "dotm", "dotx", "csv", "dbf", "DIF", "ods", "prn", "xla", "xlam", "xls", "xlsb", "xlsm", "xlsl", "xlsx", "xlt", "xltm", "xltx", "xlw", "xps", "pot", "potm", "potx", "ppa", "ppam", "pps", "ppsm", "ppsx", "ppt", "pptm", "pptx"];
    var AudiosExt = ["mp3", "ogg", "wav", "flac", "pcm", "aiff", "au", "raw", "aac", "mp4a", "wma"];
    var ImagesExt = ["jpg", "jpeg", "bmp", "gif", "pcx", "png", "tga", "tiff", "wmp"];
    var VideosExt = ["mpeg", "vob", "3gp", "mov", "mp4", "webm", "flv", "mkv", "avi", "ogm"];
    
    callback(null, file.fieldname + '-' + Date.now());
  }
});

var upload = multer({ storage: storage}).single('File');

app.post('/',function(req, res){
    upload(req, res, function(err) {
        if(err) {
            res.json({Result: 0});
        }
        res.json({Result: 1});
    });
});

module.exports = router
